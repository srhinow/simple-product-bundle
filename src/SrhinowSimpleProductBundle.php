<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

namespace Srhinow\SimpleProductBundle;


use Srhinow\SimpleProductBundle\DependencyInjection\SrhinowSimpleProductBundleExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao contao-blank-bundle.
 *
 * @author Sven Rhinow
 */
class SrhinowSimpleProductBundle extends Bundle
{
    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new SrhinowSimpleProductBundleExtension();
    }
}
