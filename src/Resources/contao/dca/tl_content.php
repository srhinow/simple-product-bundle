<?php

/**
 *
 * @copyright  Sven Rhinow 2018
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 */
/**
 * Dynamically add the parent table
 */
if (Input::get('do') == 'simple_product_entries')
{
    $GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_simple_product_entries';
}

$GLOBALS['TL_DCA']['tl_content']['palettes']['sp_product_list'] = '{type_legend},headline,type;{module_legend},product_categories,details_jumpTo;{template_legend:hide},sp_products_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['sp_product_details'] = '{type_legend},headline,type;{module_legend},list_jumpTo,product_entries;{template_legend:hide},sp_products_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
/**
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['product_categories'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'foreignKey'              => 'tl_simple_product_categories.title',
    'eval'                    => array('multiple'=>true, 'mandatory'=>true, 'chosen'=>true),
    'sql'                     => "blob NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['product_entries'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'foreignKey'              => 'tl_simple_product_entries.headline',
    'eval'                    => array('chosen'=>true, 'includeBlankOption' => true),
    'sql'                     => "blob NULL"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['details_jumpTo'] = array
(
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('fieldType'=>'radio'),
    'sql'                     => "int(10) unsigned NOT NULL default '0'",
    'relation'                => array('type'=>'hasOne', 'load'=>'eager')
);
$GLOBALS['TL_DCA']['tl_content']['fields']['list_jumpTo'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['list_jumpTo'],
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('fieldType'=>'radio'),
    'sql'                     => "int(10) unsigned NOT NULL default '0'",
    'relation'                => array('type'=>'hasOne', 'load'=>'eager')
);
$GLOBALS['TL_DCA']['tl_content']['fields']['sp_products_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['sp_products_template'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'	        	  => \Contao\Backend::getTemplateGroup('sp_item_',['sp' => ['sp_item_list','sp_item_details']]),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

