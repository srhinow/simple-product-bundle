<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

/**
 * Table tl_simple_product_entries
 */
$GLOBALS['TL_DCA']['tl_simple_product_entries'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
        'ctable'                      => array('tl_content'),
        'enableVersioning'            => true,
        'switchToEdit'                => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'alias' => 'index',
                'start,stop,published' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
            'fields'                  => array('start'),
			'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit',
		),
		'label' => array
		(
			'fields'                  => array('headline','start','stop'),
            'label_callback'          => array('tl_simple_product_entries', 'listEntries'),
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();"',
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['edit'],
                'href'                => 'table=tl_content',
				'icon'                => 'edit.gif',
			),
            'editheader' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['editheader'],
                'href'                => 'act=edit',
                'icon'                => 'header.svg'
            ),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.svg',
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if (!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'])??null . '\')) return false; Backend.getScrollOffset();"',
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif',
			)
		)
	),
	// Palettes
	'palettes' => array
	(
		'__selector__'                => array('addImage'),
		'default' => 'headline,alias;categories,teaser;{image_legend},addImage;{general_legend},published,start,stop'
	),

	// Subpalettes
	'subpalettes' => array
	(
		'addImage'                    => 'singleSRC,alt,title',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'headline' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['headline'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'alias' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['alias'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128,'tl_class'=>'w50'),
			'sql'					=> "varchar(64) NOT NULL default ''",
			'save_callback' => array
			(
				array('tl_simple_product_entries', 'generateAlias')
			)

		),
		'categories' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['categories'],
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'flag'                    => 1,
			'inputType'               => 'select',
			'foreignKey'              => 'tl_simple_product_categories.title',
            'eval'                    => array('mandatory'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
//			'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy','tl_class'=>'widget clr')
		),
		'teaser' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['teaser'],
			'exclude'                 => false,
			'search'                  => true,
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','rte'=>'tinyMCE','tl_class'=>'clr'),
			'sql'					=> "blob NULL"

		),
		'addImage' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['addImage'],
			'exclude'                 => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'singleSRC' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['singleSRC'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
			'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr','extensions' => 'jpg,jpeg,gif,png'),
			'sql'                     => "binary(16) NULL"
		),
		'alt' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['alt'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'title' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['title'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'published' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['published'],
			'exclude'                 => true,
			'filter'                  => true,
			'flag'                    => 2,
			'inputType'               => 'checkbox',
			'eval'                    => array('doNotCopy'=>true,'tl_class='=>'clr'),
			'sql'					=> "char(1) NOT NULL default ''"
		),
        'start' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['start'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_product_entries']['stop'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        )
	)
);


/**
 * Class tl_simple_product_entries
 */
class tl_simple_product_entries extends Backend
{
	
	/**
	 * Autogenerate an article alias if it has not been set yet
	 * @param mixed
	 * @param object
	 * @return string
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if (!strlen($varValue))
		{
			$autoAlias = true;
			$varValue = standardize($dc->activeRecord->headline);
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_simple_product_entries WHERE id=? OR alias=?")
								   ->execute($dc->id, $varValue);

		// Check whether the page alias exists
		if ($objAlias->numRows > 1)
		{
			if (!$autoAlias)
			{
				throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
			}

			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	}

    /**
     * List a particular record
     * @param $arrRow array
     * @return string
     */
    public function listEntries($arrRow)
    {
        $return = '<strong>'.$arrRow['headline'].'</strong> ';
        if((int) $arrRow['start'] > 0) $return .= date('d.m.Y',$arrRow['start']);
        if(((int) $arrRow['start'] > 0) && ((int) $arrRow['stop'] > 0)) $return .= ' - ';
        if((int) $arrRow['stop'] > 0) $return .= date('d.m.Y',$arrRow['stop']);

        return $return;
    }

    /**
     * Set the timestamp to 00:00:00 (see #26)
     *
     * @param integer $value
     *
     * @return integer
     */
    public function loadDate($value)
    {
        return strtotime(date('Y-m-d', $value) . ' 00:00:00');
    }
}
