<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['simple_product'] = array('Produkte');
$GLOBALS['TL_LANG']['CTE']['sp_product_list'] = array('Produkt-Liste', 'Stellt die aktiven Produkte zu den gewünschten Kategorien dar.');
$GLOBALS['TL_LANG']['CTE']['sp_product_details'] = array('Produkt-Details', 'Stellt die Details vom aktuell aufgerufenen Produkt dar.');


/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['simple_product_list'] = array('Teaser', 'Stellt die aktiven Produkte zu den gewünschten Produkten dar.');
$GLOBALS['TL_LANG']['FMD']['simple_product_details'] = array('Teaser', 'Stellt die Details vom ausgewählten Produkt dar.');


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['simple_product'] = array('Produkte', 'Die einfache Produkt-Verwaltung');
$GLOBALS['TL_LANG']['MOD']['simple_product_entries'] = array('Produkt-Einträge', 'Verwaltung der Produkte.');
$GLOBALS['TL_LANG']['MOD']['simple_product_categories'] = array('Produkt-Kategorien', 'Verwaltung der Produkt-Kategorien.');
