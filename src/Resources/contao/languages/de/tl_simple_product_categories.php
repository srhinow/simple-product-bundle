<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_simple_product_categories']['title'][0]         = 'Titel';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['title'][1]         = 'Titel der Kategorie';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['sorting'][0]		= 'Sortierung';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['sorting'][1]		= 'Sortierungsplatz in einer eventuelle Listen-Darstellung';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_product_categories']['new'][0]                          = 'Neue Kategorie';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['new'][1]                          = 'Eine neue Kategorie anlegen.';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['edit'][0]                         = 'Kategorie bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['edit'][1]                         = 'Kategorie ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['copy'][0]                         = 'Kategorie duplizieren';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['copy'][1]                         = 'Kategorie ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['delete'][0]                       = 'Kategorie löschen';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['delete'][1]                       = 'Kategorie ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['show'][0]                         = 'Kategoriedetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_product_categories']['show'][1]                         = 'Details für Kategorie ID %s anzeigen.';

