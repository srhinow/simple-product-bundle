<?php
/**
 * TL_ROOT/system/modules/simple_product/languages/de/tl_simple_product_entries.php
 *
 * Contao extension: simple_product
 * Deutsch translation file
 *
 * Copyright : &copy; Sven Rhinow <sven@sr-tag.de>
 * License   : LGPL
 * Author    : Sven Rhinow, http://www.sr-tag.de/
 * Translator: Sven Rhinow (scuM666)
 *
 */

/**
 * Global Operations
 */
$GLOBALS['TL_LANG']['tl_simple_product_entries']['properties'][0] = 'Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['properties'][1] = 'Alle Einstellungen zu den Produkten';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_simple_product_entries']['headline'][0]         = 'Produkt-Titel';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['headline'][1]         = 'geben sie den Titel für das Produkt an.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['categories'][0]			= 'Kategorien';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['categories'][1]        	= 'Ordnen Sie dem Produkt Kategorien zu.';$GLOBALS['TL_LANG']['tl_simple_product_entries']['teaser'][0]			= 'Teaser';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['teaser'][1]        	= 'Geben sie hier für eine Produkt-Übersicht einen Kurztext ein.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['addImage'][0]			= 'Produktbild anlegen';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['addImage'][1]        	= 'Geben Sie hier an ob sie ein Produktbild dem Produkt zuweisen möchten.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['singleSRC'][0]			= 'Produktbild';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['singleSRC'][1]        	= 'wählen Sie hier da passende Bild zum Produkt.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['alt'][0]			= 'Alternativer Text';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['alt'][1]        	= 'wird angezeigt wenn das Bild nicht zur Verfügung steht.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['title'][0]			= 'Bild-Titel';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['title'][1]        	= 'erscheint als Tooltip wenn sich der Mauszeiger über dem Bild aufhält.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['published'][0]			= 'Für Website-Besucher sichtbar';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['published'][1]        	= 'Geben sie hier ob das Produkt auf der Website angezeit werden soll.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['start']			= ['sichtbar ab','Wenn es erst ab einem bestimmten Tag sichtbar sein soll.'];
$GLOBALS['TL_LANG']['tl_simple_product_entries']['stop']        	= ['sichtbar bis','Wenn es nur bis zu einem bestimmten Tag sichtbar sein soll'];

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_product_entries']['new'][0]                          = 'Neues Produkt';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['new'][1]                          = 'Eine neues Produkt anlegen.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['edit'][0]                         = 'Produkt bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['edit'][1]                         = 'Produkt ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['copy'][0]                         = 'Produkt duplizieren';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['copy'][1]                         = 'Produkt ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['delete'][0]                       = 'Produkt löschen';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['delete'][1]                       = 'Produkt ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['show'][0]                         = 'Produktdetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['show'][1]                         = 'Details für Produkt ID %s anzeigen.';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_simple_product_entries']['meta_legend']        	= 'Grund-Informationen';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['recipes_text']        	= 'Produkt';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['image_legend']        	= 'Produkt-Bild';
$GLOBALS['TL_LANG']['tl_simple_product_entries']['general_legend']        	= 'weitere Einstellungen';

/**
 * Options
 */
$GLOBALS['TL_LANG']['tl_simple_product_entries']['difficulty_options'] = array(1 => 'sehr einfach', 2 => 'einfach', 3 => 'normal', 4 => 'schwer', 5 => 'sehr schwer');

