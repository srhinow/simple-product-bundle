<?php

## fields
$GLOBALS['TL_LANG']['tl_content']['product_categories'] = array('Produkt Kategorien', 'Es werden nur Produkt von hier ausgewählten Kategorien angezeigt.');
$GLOBALS['TL_LANG']['tl_content']['product_entries'] = array('Produkt Eintrag', 'Wenn statt einem GET-Parameter, immer die Einträge (Content-Elemente) von einem bestimmten Element angezeigt werden sollen.');
$GLOBALS['TL_LANG']['tl_content']['list_jumpTo'] = array('Produktlist-Seite','Wählen sie hier aus auf welcher Seite die Produkt-Liste dargestellt werden.');
$GLOBALS['TL_LANG']['tl_content']['details_jumpTo'] = array('Produktdetail-Seite','Wählen sie hier aus auf welcher Seite die Produkt-Details dargestellt werden.');
$GLOBALS['TL_LANG']['tl_content']['sp_products_template'] = array('Produkt-Template','wählen sie für die Darstellung das passende Template aus');

## Legend
$GLOBALS['TL_LANG']['tl_content']['module_legend'] = 'Modul-Einstellungen';