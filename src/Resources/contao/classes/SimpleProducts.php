<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

namespace Srhinow\SimpleProductBundle\Classes;

use Contao\Frontend;
use Contao\FrontendTemplate;
use Contao\Module;
use Model\Collection;
use Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel;

/**
 * Class TeaserManager
 */
class SimpleProducts extends \Frontend
{

    /**
     * Parse an item and return it as string
     *
     * @param $objArticle
     * @param string    $strClass
     * @param integer   $intCount
     * @param array   $settings
     *
     * @return string
     */
    protected function parseArticle($objArticle, $strClass='', $intCount=0, $settings)
    {
        /** @var FrontendTemplate|object $objTemplate */
        $objTemplate = new FrontendTemplate($settings['template']);
        $objTemplate->setData($objArticle->row());

        if ($objArticle->cssClass != '')
        {
            $strClass = ' ' . $objArticle->cssClass . $strClass;
        }


        $objTemplate->class = $strClass;
        $objTemplate->productHeadline = $objArticle->headline;
        $objTemplate->linkHeadline = $this->generateLink($objArticle->headline, $objArticle, $settings);
        $objTemplate->more = $this->generateLink($GLOBALS['TL_LANG']['MSC']['more'], $objArticle, $settings);
        $objTemplate->href = $this->generateUrl($objArticle, $settings);
        $objTemplate->count = $intCount; // see #5708
        $objTemplate->arrEntries = [];
        $objTemplate->hasText = false;
        $objTemplate->hasTeaser = false;

        // Clean the RTE output
        if ($objArticle->teaser != '')
        {
            $objTemplate->hasTeaser = true;
            $objTemplate->teaser = \StringUtil::toHtml5($objArticle->teaser);
            $objTemplate->teaser = \StringUtil::encodeEmail($objTemplate->teaser);
        }

        $id = $objArticle->id;

        $objTemplate->arrEntries = function () use ($id)
        {
            $arrEntries = [];
            $objElement = \ContentModel::findPublishedByPidAndTable($id, 'tl_simple_product_entries');

            if ($objElement !== null)
            {
                while ($objElement->next())
                {

                    $arrEntries[] = \Contao\Controller::getContentElement($objElement->current());
                }
            }

            return $arrEntries;
        };

        $objTemplate->hasText = function () use ($objArticle)
        {
            return \ContentModel::countPublishedByPidAndTable($objArticle->id, 'tl_simple_products') > 0;
        };


        // Add the meta information
        $objTemplate->timestamp = $objArticle->date;
        $objTemplate->datetime = date('Y-m-d\TH:i:sP', $objArticle->date);

        $objTemplate->addImage = false;

        // Add an image
        if ($objArticle->addImage && $objArticle->singleSRC != '')
        {
            $objModel = \FilesModel::findByUuid($objArticle->singleSRC);

            if ($objModel !== null && is_file(TL_ROOT . '/' . $objModel->path))
            {
                // Do not override the field now that we have a model registry (see #6303)
                $arrArticle = $objArticle->row();

                // Override the default image size
                if ($this->imgSize != '')
                {
                    $size = \StringUtil::deserialize($this->imgSize);

                    if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]))
                    {
                        $arrArticle['size'] = $this->imgSize;
                    }
                }

                $arrArticle['singleSRC'] = $objModel->path;
                $this->addImageToTemplate($objTemplate, $arrArticle, null, null, $objModel);
            }
        }

        return $objTemplate->parse();
    }

    /**
     * Parse one or more items and return them as array
     * @param Model|Collection $objArticles
     * @param integer $countItems
     * @param array   $settings
     * @return array
     */
    public function parseProducts($objArticles, $countItems, $settings)
    {
        $limit = $countItems;

        if ($limit < 1)
        {
            return array();
        }

        $count = 0;
        $arrArticles = array();

        while ($objArticles->next())
        {
            $objArticle = $objArticles->current();
            $strClass = ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even');
            $arrArticles[] = $this->parseArticle($objArticle, $strClass, $count, $settings);
        }

        return $arrArticles;
    }

    /**
     * Parse one item and return them as string
     * @param Model|Collection $objArticles
     * @param array   $settings
     * @return string
     */
    public function parseProduct($objArticle, $settings)
    {

        if(!is_object($objArticle))
        {
            return '';
        }

        return $this->parseArticle($objArticle, '', 1, $settings);

    }

    /**
     * Generate a link and return it as string
     *
     * @param string    $strLink
     * @param Collection $objArticle
     * @param array $settings
     *
     * @return string
     */
    protected function generateLink($strLink, $objArticle, $settings)
    {
        $strUrl = '';
        $objPage = \PageModel::findByPk($settings['details_jumpTo']);

        if(null !== $objPage) $strUrl = ampersand($objPage->getFrontendUrl((\Config::get('useAutoItem') ? '/' : '/items/') . ($objArticle->alias ?: $objArticle->id)));

        // Internal link
        return sprintf('<a href="%s" title="%s" itemprop="url">%s</a>',
            $strUrl,
            \StringUtil::specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['readMore'], $objArticle->headline), true),
            $strLink
        );
    }

    /**
     * Generate a url to detailpage and return it as string
     *
     * @param Collection $objArticle
     * @param array $settings
     *
     * @return string
     */
    protected function generateUrl($objArticle, $settings)
    {
        $strUrl = '';
        $objPage = \PageModel::findByPk($settings['details_jumpTo']);

        if(null !== $objPage) $strUrl = ampersand($objPage->getFrontendUrl((\Config::get('useAutoItem') ? '/' : '/items/') . ($objArticle->alias ?: $objArticle->id)));

        return $strUrl;
    }
}
