<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

namespace Srhinow\SimpleProductBundle\Elements;


use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\StringUtil;
use Contao\System;
use Patchwork\Utf8;
use Srhinow\SimpleProductBundle\Classes\SimpleProducts;
use Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel;

/**
 * Class ContentProductList
 *
 * @property array  product_categories
 */
class ContentProductList extends ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_sp_productlist';


	/**
	 * Display a wildcard in the back end
	 *
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{

			/** @var \BackendTemplate|object $objTemplate */
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### ' . strtoupper($GLOBALS['TL_LANG']['CTE']['sp_product_list'][0]) . ' ###';
			$objTemplate->title = $this->headline;
			return $objTemplate->parse();
		}

        $this->product_categories = StringUtil::deserialize($this->product_categories);

        // Return if there are no categories
        if (empty($this->product_categories) || !\is_array($this->product_categories))
        {
            return '';
        }

		if(strlen($this->customTpl) >0) $this->strTemplate = $this->customTpl;

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{

        $intTotal =  SimpleProductEntriesModel::countPublishedByCategories($this->product_categories);

        if ($intTotal < 1)
        {
            return;
        }

        $objProducts = SimpleProductEntriesModel::findPublishedByCategories($this->product_categories);

        $this->Template->articles = array();
        $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];


        $SpClass = new SimpleProducts();

        $settings = array(
            'categories' => $this->product_categories,
            'details_jumpTo' => $this->details_jumpTo,
            'template' => $this->sp_products_template
        );

        // Add the projects
        if ($objProducts !== null)
        {
            $this->Template->products = $SpClass->parseProducts($objProducts, $intTotal, $settings);
        }

        $this->Template->categories = $this->product_categories;
	}
}
