<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

namespace Srhinow\SimpleProductBundle\Elements;


use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\Input;
use Patchwork\Utf8;
use Srhinow\SimpleProductBundle\Classes\SimpleProducts;
use Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel;

/**
 * Class ContentProductDetails
 *
 * @property array  product_categories
 */
class ContentProductDetails extends ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_sp_productdetails';


	/**
	 * Display a wildcard in the back end
	 *
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			/** @var \BackendTemplate|object $objTemplate */
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### ' . strtoupper($GLOBALS['TL_LANG']['CTE']['sp_product_details'][0]) . ' ###';
			$objTemplate->title = $this->headline;
			return $objTemplate->parse();
		}

        // Set the item from the auto_item parameter
        if (!isset($_GET['product']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            Input::setGet('product', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no news item has been specified
        if (!Input::get('product') && !isset($this->product_entries))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
        global $objPage;

        $productIdentifier = (int) $this->product_entries;
        $getProductParameter = Input::get('product');
        if (isset($getProductParameter)) $productIdentifier = Input::get('product');

        // Get the total number of items
        $objProduct = SimpleProductEntriesModel::findByIdOrAlias($productIdentifier);

        if ($objProduct === null)
        {
            // Do not index or cache the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            $this->Template->articles = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('product')) . '</p>';
            return;
        }

        $SpClass = new SimpleProducts();

        $settings = array(
            'categories' => $this->product_categories,
            'details_jumpTo' => $this->details_jumpTo,
            'template' => $this->sp_products_template
        );

        // Add the projects
        if ($objProduct !== null)
        {
            $this->Template->product = $SpClass->parseProduct($objProduct, $settings);
        }

        // Overwrite the page title (see #2853 and #4955)
        if ($objProduct->headline != '')
        {
            $objPage->pageTitle = strip_tags(strip_insert_tags($objProduct->headline));
        }

        // Overwrite the page description
        if ($objProduct->teaser != '')
        {
            $objPage->description = $this->prepareMetaDescription($objProduct->teaser);
        }
	}
}
