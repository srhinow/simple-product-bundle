<?php
/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

$GLOBALS['BE_SIMPLE_PRODUCT_BUNDLE']['PROPERTIES']['PUBLICSRC'] = "bundles/srhinowsimpleproduktbundle";
/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD'], 1, array('simple_product' => array()));

$GLOBALS['BE_MOD']['simple_product']['simple_product_categories'] = array
(
    'tables' => array('tl_simple_product_categories'),
    'icon'   => $GLOBALS['BE_SIMPLE_PRODUCT_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/category.png'
);

$GLOBALS['BE_MOD']['simple_product']['simple_product_entries'] = array
(
    'tables' => array('tl_simple_product_entries','tl_content'),
    'icon'   => $GLOBALS['BE_SIMPLE_PRODUCT_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/cutlery.png'
);

/**
 * -------------------------------------------------------------------------
 * CONTENT ELEMENT
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['TL_CTE'], 1, array('simple_product' => array()));
$GLOBALS['TL_CTE']['simple_product']['sp_product_list'] = 'Srhinow\SimpleProductBundle\Elements\ContentProductList';
$GLOBALS['TL_CTE']['simple_product']['sp_product_details'] = 'Srhinow\SimpleProductBundle\Elements\ContentProductDetails';

/**
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
    'Recipes' => array
    (
        'simple_product_list'    => 'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesList',
        'simple_product_details'    => 'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesDetails',
    )
));

$GLOBALS['TL_MODELS']['tl_simple_product_entries'] = \Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel::class;


/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
//$GLOBALS['TL_HOOKS']['getSearchablePages'][] = array('Srhinow\SimpleProductBundle\Hooks\SimpleProductHooks', 'getSearchablePages');

/**
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
// $GLOBALS['TL_PERMISSIONS'][] = 'simple_product_modules';
$GLOBALS['BM']['PROPERTIES']['ID'] = 1;

