<?php
namespace Srhinow\SimpleProductBundle\Models;

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

use Contao\Model;

/**
 * Reads and writes Product-Categorie Items
 */

class SimpleProductCategoriesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_product_categories';

	/**
	 * Find all open recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findRecipes($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if(is_array($arrIds) && count($arrIds) > 0)
		{
			$arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
		}

		$arrColumns[] = "$t.open='1'";

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.headline ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;


		return static::findBy($arrColumns, null, $arrOptions);
	}
	/**
	 * Count all recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function countRecipesByOpen(array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if (!BE_USER_LOGGED_IN)
		{
			$arrColumns[] = "$t.open='1'";
		}

		return static::countBy($arrColumns, null, $arrOptions);
	}
}
