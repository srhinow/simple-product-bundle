<?php
namespace Srhinow\SimpleProductBundle\Models;

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */


use Contao\Date;
use Contao\Model;

/**
 * Reads and writes Product Items
 *
 */


class SimpleProductEntriesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_product_entries';

	/**
	 * Find all active and published product items
	 *
     * @param int $intLimit
     * @param int $intOffset
     * @param array $arrIds
     * @param array $arrOptions
     * @return Model\Collection|SimpleProductEntriesModel|null
	 */
	public static function findProducts($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if(is_array($arrIds) && count($arrIds) > 0)
		{
			$arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
		}

        if (!BE_USER_LOGGED_IN)
        {
            $time = Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.headline ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;


		return static::findBy($arrColumns, null, $arrOptions);
	}

    /**
     * Find published product items by their parent IDs
     * @param $arrCategories
     * @param int $intLimit
     * @param int $intOffset
     * @param array $arrOptions
     * @return Model\Collection|\Model\Collection|SimpleProductEntriesModel|null
     */
    public static function findPublishedByCategories($arrCategories, $intLimit=0, $intOffset=0, array $arrOptions=array())
    {
        if (!is_array($arrCategories) || empty($arrCategories))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = array("$t.categories IN(" . implode(',', array_map('intval', $arrCategories)) . ")");

        // Never return unpublished elements in the back end, so they don't end up in the RSS feed
        if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order']  = "$t.headline ASC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }


    /**
     * Count published product items by their category-IDs
     *
     * @param array   $arrCategories     An array of product category IDs
     * @param array   $arrOptions  An optional options array
     *
     * @return integer The number of news items
     */
    public static function countPublishedByCategories($arrCategories, array $arrOptions=array())
    {
        if (!is_array($arrCategories) || empty($arrCategories))
        {
            return 0;
        }

        $t = static::$strTable;
        $arrColumns = array("$t.categories IN(" . implode(',', array_map('intval', $arrCategories)) . ")");


        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }


    /**
     * Count all product items
     *
     * @param array $arrOptions
     * @return int
     */
    public static function countArchiveProductsByOpen(array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = null;

        if (!BE_USER_LOGGED_IN)
        {
            $time = Date::floorToMinute();
            $arrColumns[] = " ($t.start='' OR $t.start <= '" . ($time) . "') AND $t.published='1'";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
