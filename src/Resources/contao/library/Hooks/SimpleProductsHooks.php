<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple-product-bundle
 * @license    LGPL
 * @filesource
 */

namespace Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel;

use Contao\PageModel;
use Contao\Frontend;
use Srhinow\SimpleProductBundle\Models\SimpleProductEntriesModel;

class SimpleRecipesHooks extends Frontend
{
	/**
	 * Add store items to the indexer
	 *
	 * @param array   $arrPages
	 * @param integer $intRoot
	 * @param boolean $blnIsSitemap
	 *
	 * @return array
	 */
	public function getSearchablePages($arrPages, $intRoot=0, $blnIsSitemap=false)
	{
        $arrPages = array();
        $strBase = '';


		$arrProcessed = array();
		$time = \Date::floorToMinute();

		$props = SimpleProductEntriesModel::findPropertiesById(1);

		// Walk through each archive
		if ($props !== null)
		{
			// Skip if properties without target page
			if (!$props->jumpTo)
			{
				return $arrPages;
			}

			$itemsObj = SimpleRecipesEntriesModel::findRecipes();

			if($itemsObj !== null)
			{
				$objParent = PageModel::findWithDetails($props->jumpTo);

				// Set the domain (see #6421)
				$domain = ($objParent->rootUseSSL ? 'https://' : 'http://') . ($objParent->domain ?: \Environment::get('host')) . TL_PATH . '/';
				$strUrl = $domain . $this->generateFrontendUrl($objParent->row(), ((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ?  '/%s' : '/items/%s'), $objParent->language);

				while($itemsObj->next())
				{
					// Link to the default page
					$arrPages[] = $strBase . sprintf($strUrl, (($itemsObj->alias != '' && !\Config::get('disableAlias')) ? $itemsObj->alias : $itemsObj->id));
				}

			}
		}

		return $arrPages;
	}

}
