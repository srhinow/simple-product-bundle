# Changes in srhinow/simple-product-bundle
## 1.0 (19.06.2023)
- Content-Element in Detailansicht als Array bereitstellen damit man sie manipulieren kann.

## 0.0.2 (29.12.22)
- fix ce_sp_productlist.html5 if products not set

## 0.0.1 (27.01.19)
- erste lauffähige Version